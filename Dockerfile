FROM ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive

RUN \
	apt-get update -qq && \
	apt-get install -y -qqq \
		libuv1-dev \
		meson \
		gcc \
		pkg-config \
		dumb-init \
		git && \
	rm -rf /var/lib/apt/lists/*

RUN \
	git clone https://gitlab.labs.nic.cz/knot/echo-server.git && \
	cd echo-server && \
	meson build && \
	ninja -C build && \
	cd ..

ENTRYPOINT ["dumb-init", "echo-server/build/echo-server"]
