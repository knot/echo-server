// vim: set ts=2 sw=2 et:
/* Copyright Joyent, Inc. and other Node contributors. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "task.h"
#include <assert.h>
#include <getopt.h>
#include <uv.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

static char default_ip6[] = "::1";
static uint16_t default_port = 53536;
static uint16_t default_forks = 1;

struct args {
	const char *ip6;
	uint16_t port;
  uint16_t forks;
};

typedef struct {
  uv_write_t req;
  uv_buf_t buf;
} write_req_t;

static uv_loop_t* loop;

// static int server_closed;
static uv_tcp_t tcpServer;
static uv_udp_t udpServer;
static uv_handle_t* server;
static uv_udp_send_t* send_freelist;

static void after_write(uv_write_t* req, int status);
static void after_read(uv_stream_t*, ssize_t nread, const uv_buf_t* buf);
static void on_close(uv_handle_t* peer);
// static void on_server_close(uv_handle_t* handle);
static void on_connection(uv_stream_t*, int status);


static void after_write(uv_write_t* req, int status) {
  write_req_t* wr;

  /* Free the read/write buffer and the request */
  wr = (write_req_t*) req;
  free(wr->buf.base);
  free(wr);

  if (status == 0)
    return;

  fprintf(stderr,
          "uv_write error: %s - %s\n",
          uv_err_name(status),
          uv_strerror(status));
}


static void after_shutdown(uv_shutdown_t* req, int status) {
  uv_close((uv_handle_t*) req->handle, on_close);
  free(req);
}


static void after_read(uv_stream_t* handle,
                       ssize_t nread,
                       const uv_buf_t* buf) {
  write_req_t *wr;
  uv_shutdown_t* sreq;

  if (nread < 0) {
    free(buf->base);
    sreq = malloc(sizeof* sreq);
    ASSERT(0 == uv_shutdown(sreq, handle, after_shutdown));
    return;
  }

  if (nread == 0) {
    /* Everything OK, but nothing read. */
    free(buf->base);
    return;
  }

  wr = (write_req_t*) malloc(sizeof *wr);
  ASSERT(wr != NULL);
  wr->buf = uv_buf_init(buf->base, nread);

  if (uv_write(&wr->req, handle, &wr->buf, 1, after_write)) {
    FATAL("uv_write failed");
  }
}


static void on_close(uv_handle_t* peer) {
  free(peer);
}


static void echo_alloc(uv_handle_t* handle,
                       size_t suggested_size,
                       uv_buf_t* buf) {
  buf->base = malloc(suggested_size);
  buf->len = suggested_size;
}

static void slab_alloc(uv_handle_t* handle,
                       size_t suggested_size,
                       uv_buf_t* buf) {
  /* up to 16 datagrams at once */
  static char slab[16 * 64 * 1024];
  buf->base = slab;
  buf->len = sizeof(slab);
}

static void on_connection(uv_stream_t* server, int status) {
  uv_stream_t* stream;
  int r;

  if (status != 0) {
    fprintf(stderr, "Connect error %s\n", uv_err_name(status));
  }
  ASSERT(status == 0);

  stream = malloc(sizeof(uv_tcp_t));
  ASSERT(stream != NULL);
  r = uv_tcp_init(loop, (uv_tcp_t*)stream);
  ASSERT(r == 0);

  /* associate server with stream */
  stream->data = server;

  r = uv_accept(server, stream);
  ASSERT(r == 0);

  r = uv_read_start(stream, echo_alloc, after_read);
  ASSERT(r == 0);
}


// static void on_server_close(uv_handle_t* handle) {
//   ASSERT(handle == server);
// }

static uv_udp_send_t* send_alloc(void) {
  uv_udp_send_t* req = send_freelist;
  if (req != NULL)
    send_freelist = req->data;
  else
    req = malloc(sizeof(*req));
  return req;
}

static void on_send(uv_udp_send_t* req, int status) {
  ASSERT(req != NULL);
  ASSERT(status == 0);
  req->data = send_freelist;
  send_freelist = req;
}

static void on_recv(uv_udp_t* handle,
                    ssize_t nread,
                    const uv_buf_t* rcvbuf,
                    const struct sockaddr* addr,
                    unsigned flags) {
  uv_buf_t sndbuf;

  if (nread == 0) {
    /* Everything OK, but nothing read. */
    return;
  }

  ASSERT(nread > 0);

  uv_udp_send_t* req = send_alloc();
  ASSERT(req != NULL);
  sndbuf = uv_buf_init(rcvbuf->base, nread);
  ASSERT(0 <= uv_udp_send(req, handle, &sndbuf, 1, addr, on_send));
}

// static int tcp4_echo_start(int port) {
//   struct sockaddr_in addr;
//   int r;
//
//   ASSERT(0 == uv_ip4_addr("0.0.0.0", port, &addr));
//
//   server = (uv_handle_t*)&tcpServer;
//
//   r = uv_tcp_init(loop, &tcpServer);
//   if (r) {
//     /* TODO: Error codes */
//     fprintf(stderr, "Socket creation error\n");
//     return 1;
//   }
//
//   r = uv_tcp_bind(&tcpServer, (const struct sockaddr*) &addr, 0);
//   if (r) {
//     /* TODO: Error codes */
//     fprintf(stderr, "Bind error\n");
//     return 1;
//   }
//
//   r = uv_listen((uv_stream_t*)&tcpServer, SOMAXCONN, on_connection);
//   if (r) {
//     /* TODO: Error codes */
//     fprintf(stderr, "Listen error %s\n", uv_err_name(r));
//     return 1;
//   }
//
//   return 0;
// }


static int tcp6_echo_start(const char* ip, int port) {
  struct sockaddr_in6 addr6;
  int r;

  ASSERT(0 == uv_ip6_addr(ip, port, &addr6));

  server = (uv_handle_t*)&tcpServer;

  r = uv_tcp_init(loop, &tcpServer);
  if (r) {
    /* TODO: Error codes */
    fprintf(stderr, "Socket creation error\n");
    return 1;
  }

  const int fd = socket(addr6.sin6_family, SOCK_STREAM, 0);
  int yes = 1;
  assert(setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes)) == 0);

  assert(uv_tcp_open(&tcpServer, fd) == 0);

  /* IPv6 is optional as not all platforms support it */
  r = uv_tcp_bind(&tcpServer, (const struct sockaddr*) &addr6, 0);
  if (r) {
    /* show message but return OK */
    fprintf(stderr, "IPv6 failed to bind: %s\n", uv_strerror(r));
    return 0;
  }

  r = uv_listen((uv_stream_t*)&tcpServer, SOMAXCONN, on_connection);
  if (r) {
    /* TODO: Error codes */
    fprintf(stderr, "Listen error\n");
    return 1;
  }

  return 0;
}

static int udp6_echo_start(const char* ip, int port) {
  struct sockaddr_in6 addr6;
  int r;

  ASSERT(0 == uv_ip6_addr(ip, port, &addr6));

  server = (uv_handle_t*)&udpServer;

  r = uv_udp_init(loop, &udpServer);
  if (r) {
    fprintf(stderr, "uv_udp_init: %s\n", uv_strerror(r));
    return 1;
  }

  const int fd = socket(addr6.sin6_family, SOCK_DGRAM, 0);
  int yes = 1;
  assert(setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes)) == 0);

  assert(uv_udp_open(&udpServer, fd) == 0);

  r = uv_udp_bind(&udpServer, (const struct sockaddr*) &addr6, 0);
  if (r) {
    fprintf(stderr, "uv_udp_bind: %s\n", uv_strerror(r));
    return 1;
  }

  r = uv_udp_recv_start(&udpServer, slab_alloc, on_recv);
  if (r) {
    fprintf(stderr, "uv_udp_recv_start: %s\n", uv_strerror(r));
    return 1;
  }

  return 0;
}


// static int udp4_echo_start(int port) {
//   struct sockaddr_in addr;
//   int r;
//
//   ASSERT(0 == uv_ip4_addr("127.0.0.1", port, &addr));
//   server = (uv_handle_t*)&udpServer;
//
//   r = uv_udp_init(loop, &udpServer);
//   if (r) {
//     fprintf(stderr, "uv_udp_init: %s\n", uv_strerror(r));
//     return 1;
//   }
//
//   r = uv_udp_bind(&udpServer, (const struct sockaddr*) &addr, 0);
//   if (r) {
//     fprintf(stderr, "uv_udp_bind: %s\n", uv_strerror(r));
//     return 1;
//   }
//
//   r = uv_udp_recv_start(&udpServer, slab_alloc, on_recv);
//   if (r) {
//     fprintf(stderr, "uv_udp_recv_start: %s\n", uv_strerror(r));
//     return 1;
//   }
//
//   return 0;
// }

void help(char *argv[])
{
  printf("Usage: %s [parameters]\n", argv[0]);
  printf("\nParameters:\n"
         " -6, --ip6=[addr]     Server address to bind to (default: %s).\n"
         " -p, --port=[port]     Server port to bind to (default: %u).\n"
         " -f, --forks=[N]     Number of forks (default: %u).\n"
         ,
     default_ip6,
     default_port,
     default_forks);
}

void init_args(struct args* args)
{
  args->ip6 = default_ip6;
  args->port = default_port;
  args->forks = default_forks;
}

int main(int argc, char **argv)
{
  long int li_value = 0;
  int c = 0, li = 0;
  struct option opts[] = {
    {"ip6",       required_argument, 0, '6'},
    {"port",       required_argument, 0, 'p'},
    {"forks",       required_argument, 0, 'f'}
  };
  struct args args;
  init_args(&args);
  while ((c = getopt_long(argc, argv, "6:p:f:", opts, &li)) != -1) {
    switch (c) {
    case '6':
      args.ip6 = optarg;
      break;
    case 'p':
      li_value = strtol(optarg, NULL, 10);
      if (li_value <= 0 || li_value > UINT16_MAX) {
        printf("error: '-p' requires a positive"
            " number less or equal to 65535, not '%s'\n", optarg);
        return -1;
      }
      args.port = (uint16_t)li_value;
      break;
    case 'f':
      li_value = strtol(optarg, NULL, 10);
      if (li_value <= 0 || li_value > UINT16_MAX) {
        printf("error: '-f' requires a positive"
            " number less or equal to 65535, not '%s'\n", optarg);
        return -1;
      }
      args.forks = (uint16_t)li_value;
      break;
    default:
      help(argv);
      return -1;
    }
  }

  int forks = args.forks;

  while (--forks > 0) {
		int pid = fork();
    if (pid < 0)
      return 1;
    else if (pid == 0)
      break;
  }

  loop = uv_default_loop();

  if (tcp6_echo_start(args.ip6, args.port))
    return 1;
  if (udp6_echo_start(args.ip6, args.port))
    return 1;

  uv_run(loop, UV_RUN_DEFAULT);
  return 0;
}
