High-Performance Echo Server
============================

IPv6 echo server that supports UDP and TCP using libuv.

Build
-----

Requires libuv-devel, meson.

```
meson build
ninja -C build
```

Usage
-----

```
Usage: ./build/echo-server [parameters]

Parameters:
 -6, --ip6=[addr]     Server address to bind to (default: ::1).
 -p, --port=[port]     Server port to bind to (default: 53536).
 -f, --forks=[N]     Number of forks (default: 1).
```

Docker
------

```
docker run --network host registry.labs.nic.cz/knot/echo-server:latest -6 ::1 -p 53536 -f 4
```
